msgid ""
msgstr ""
"Project-Id-Version: GPSBabelGUI-0.2.15.0-it\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Michele Locati <mlocati@tiscali.it>\n"
"Language-Team: ing. Michele Locati <mlocati@tiscali.it>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Italian\n"
"X-Poedit-Country: ITALY\n"
"X-Poedit-SourceCharset: utf-8\n"

msgid "(integer sec or 'auto') Barograph to GPS time diff"
msgstr "(sec interi o 'auto') Differenza temporale tra il barografo e il GPS"

msgid "(USR input) Break segments into separate tracks"
msgstr "(ingresso USR) suddividi i segmenti in tracce separate"

msgid "(USR output) Merge into one segmented track"
msgstr "(destinazione USR) Unisci tracce segmentate in una traccia"

msgid "Ad-hoc closed icon name"
msgstr "Nome ad-hoc icona chiusa"

msgid "Ad-hoc open icon name"
msgstr "Nome ad-hoc icona aperta"

msgid "After output job done sleep n second(s)"
msgstr "Attendi n secondi dopo il termine dell'elaborazione"

msgid "Allow whitespace synth. shortnames"
msgstr "Permetti spazi nei nomi brevi sintetizzati"

msgid "Altitudes are absolute and not clamped to ground"
msgstr "Le quote sono assolute e non relative al terreno"

msgid "Append icon_descr to description"
msgstr "Aggiungi icon_descr alla descrizione"

msgid "Append realtime positioning data to the output file instead of truncating"
msgstr "Aggiungi le informazioni sulla posizione in realtime all'uscita invece che troncarle"

msgid "Base URL for link tag in output"
msgstr "Url di base per i tag link in uscita"

msgid "Basename prepended to URL on output"
msgstr "Nome base anteposto all'url in uscita"

msgid "Bitmap of categories"
msgstr "Bitmap delle categorie"

msgid "Category name (Cache)"
msgstr "Nome categoria (cache)"

msgid "Category number to use for written waypoints"
msgstr "Numero categoria da usare per i punti d'interesse scritti"

msgid "Color for lines or mapnotes"
msgstr "Colore per le linee o le note della mappa"

msgid "Command unit to power itself down"
msgstr "Chiedi al dispositivo di spegnersi"

msgid "Complete date-free tracks with given date (YYYYMMDD)."
msgstr "Completa le tracce senza data con la data fornita (AAAAMMGG)."

msgid "Create unique waypoint names (default = yes)"
msgstr "Crea punti d'interesse con nomi univoci (predef.: yes)"

msgid "Create waypoints from geocache log entries"
msgstr "Crea punti d'interesse dagli elementi nel registro geocache"

msgid "Database name"
msgstr "Nome banca dati"

msgid "Database name (filename)"
msgstr "Nome banca dati (nome file)"

msgid "Datum (default=NAD27)"
msgstr "Datum (predef.=NAD27)"

msgid "Days after which points are considered old"
msgstr "Giorni dopo i quali i punti sono considerati vecchi"

msgid "Decimal seconds to pause between groups of strings"
msgstr "Decimi di secondo di pausa tra gruppi di stringhe"

msgid "Default category on output"
msgstr "Categoria predefinita in uscita"

msgid "Default category on output (1..16)"
msgstr "Categoria predefinita in uscita (1... 16)"

msgid "Default icon name"
msgstr "Nome icona predefinita"

msgid "Default location"
msgstr "Posizione predefinita"

msgid "Default proximity"
msgstr "Prossimità predefinita"

msgid "Default speed"
msgstr "Velocità predefinita"

msgid "Default speed for waypoints (knots/hr)"
msgstr "Velocità predefinita per i punti di interesse (nodi/ora)"

msgid "Degrees output as 'ddd', 'dmm'(default) or 'dms'"
msgstr "Gradi in uscita come 'ggg', 'gmm' (predef.) o 'gms'"

msgid "Delete all routes"
msgstr "Cancella tutte le rotte"

msgid "Delete all track points"
msgstr "Cancella tutti i punti traccia"

msgid "Delete all waypoints"
msgstr "Cancella tutti i punti d'interesse"

msgid "Display labels on track and routepoints  (default = 1)"
msgstr "Mostra le etichette su tracce e punti rotta (predef.=1)"

msgid "Distance unit [m=metric, s=statute]"
msgstr "Unità di lunghezza (m=metrico, s=statutario)"

msgid "Do not add geocache data to description"
msgstr "Non aggiungere dati geocache alla descrizione"

msgid "Do not add URLs to description"
msgstr "Non aggiungere url alla descrizione"

msgid "Don't show gpi bitmap on device"
msgstr "Non mostrare la bitmap gpi sul dispositivo"

msgid "Draw extrusion line from trackpoint to ground"
msgstr "Disegna linee estruse dai punti traccia al terreno"

msgid "Drop route points that do not have an equivalent waypoint (hidden points)"
msgstr "Ometti i punti rotta che non hanno un punto di interesse equivalente (punti nascosti)"

msgid "Enable alerts on speed or proximity distance"
msgstr "Abilita avvisi su velocità o prossimità"

msgid "Encrypt hints using ROT13"
msgstr "Cripta le note usando ROT13"

msgid "Encrypt hints with ROT13"
msgstr "Cripta le note con ROT13"

msgid "Erase device data after download"
msgstr "Cancella i dati dal dispositivo dopo lo scaricamento"

msgid "Export linestrings for tracks and routes"
msgstr "Esporta le linee stringa da tracce e rotte"

msgid "Export placemarks for tracks and routes"
msgstr "Esporta segnaposti da tracce e rotte"

msgid "Full path to XCSV style file"
msgstr "Percorso completo del file di stile XCSV"

msgid "Generate # points"
msgstr "N° punti da creare"

msgid "Generate file with lat/lon for centering map"
msgstr "Crea un file con lat/lon per centrare la mappa"

msgid "Give points (waypoints/route points) a default radius (proximity)"
msgstr "Raggio predefinito (prossimità) dei punti (d'interesse/delle rotte)"

msgid "GPS datum (def. WGS 84)"
msgstr "Datum GPS (predef. WGS 84)"

msgid "Height in pixels of map"
msgstr "Altezza in pixel della mappa"

msgid "Ignore event marker icons on read"
msgstr "Ignora le icone dei marcatori in fase di lettura"

msgid "Include extended data for trackpoints (default = 1)"
msgstr "Includi dati estesi sui punti traccia (predef.=1)"

msgid "Include groundspeak logs if present"
msgstr "Includi registri groundspeak se presenti"

msgid "Include major turn points (with description) from calculated route"
msgstr "Includi i punti di svolta principali (con descrizione) dalla rotta calcolata"

msgid "Include only via stations in route"
msgstr "Includi solo stazioni di via nella rotta"

msgid "Include short name in bookmarks"
msgstr "Includi nomi brevi dei segnaposti"

msgid "Index of name field in .dbf"
msgstr "Indice del campo nome del .dbf"

msgid "Index of route (if more the one in source)"
msgstr "Indice della rotta (se nell'origine ce nè più d'una)"

msgid "Index of route to write (if more the one in source)"
msgstr "Indice della rotta da scrivere (se nell'origine ce nè più d'una)"

msgid "Index of route/track to write (if more the one in source)"
msgstr "Indice della rotta/traccia da scrivere (se nell'origine ce nè più d'una)"

msgid "Index of track (if more the one in source)"
msgstr "Indice della traccia (se nell'origine ce nè più d'una)"

msgid "Index of track to write (if more the one in source)"
msgstr "Indice della traccia da scrivere (se nell'origine ce nè più d'una)"

msgid "Index of URL field in .dbf"
msgstr "Indice del campo url nel .dbf"

msgid "Indicate direction of travel in track icons (default = 0)"
msgstr "Indica la direzione di viaggio nelle icone di traccia (predef.=0)"

msgid "Infrastructure closed icon name"
msgstr "Nome icona per infrastruttura chiusa"

msgid "Infrastructure open icon name"
msgstr "Nome icona per infrastruttura aperta"

msgid "Keep turns if simplify filter is used"
msgstr "Mantieni le svolte se si usa il filtro di semplificazione"

msgid "Length of generated shortnames"
msgstr "Lunghezza dei nomi brevi generati"

msgid "Length of generated shortnames (default 16)"
msgstr "Lunghezza dei nomi brevi generati (predef. 16)"

msgid "Line color, specified in hex AABBGGRR"
msgstr "Colore delle linee, indicato in esadecimale AABBVVRR"

msgid "Make synth. shortnames unique"
msgstr "I nomi brevi sintetizzati vengono generati univoci"

msgid "MapSend version TRK file to generate (3,4)"
msgstr "Versione del file TRK di MapSen da generare (3, 4)"

msgid "Margin for map.  Degrees or percentage"
msgstr "Margine della mappa. Gradi o percentuale"

msgid "Marker type for new points"
msgstr "Tipo marcatore per i nuovi punti"

msgid "Marker type for old points"
msgstr "Tipo marcatore per i vecchi punti"

msgid "Marker type for unfound points"
msgstr "Tipo marcatore per i punti non trovati"

msgid "Max length of waypoint name to write"
msgstr "Lunghezza massima dei nomi dei punti d'interesse da scrivere"

msgid "Max number of comments to write (maxcmts=200)"
msgstr "Numero massimo di commenti da scrivere (maxcmts=200)"

msgid "Max shortname length when used with -s"
msgstr "Lunghezza massima dei nomi brevi quando si usa -s"

msgid "Max synthesized shortname length"
msgstr "Lunghezza massima dei nomi brevi sintetizzati"

msgid "Merge output with existing file"
msgstr "Unisci la destinazione con un file esistente"

msgid "MTK compatible CSV output file"
msgstr "FIle di destinazione CVS compatibile con MTK"

msgid "Name of the 'unassigned' category"
msgstr "Nome della categoria 'Non assegnato'"

msgid "New name for the route"
msgstr "Nuovo nome della rotta"

msgid "No separator lines between waypoints"
msgstr "Nessuna linea di separazione tra punti d'interesse"

msgid "No whitespace in generated shortnames"
msgstr "Nessuno spazio nei nomi brevi generati"

msgid "Non-stealth encrypted icon name"
msgstr "Nome codificato dell'icona non nascosta"

msgid "Non-stealth non-encrypted icon name"
msgstr "Nome non codificato dell'icona non nascosta"

msgid "Numeric value of bitrate (baud=4800)"
msgstr "Valore numerico del bitrate (baud=4800)"

msgid "Omit Placer name"
msgstr "Ometti nome localizzatore"

msgid "Only read turns; skip all other points"
msgstr "Leggi solo le svolte; tralascia tutti gli altri punti"

msgid "Path to HTML style sheet"
msgstr "Percorso del foglio di stile html"

msgid "Precision of coordinates"
msgstr "Precisione delle coordinate"

msgid "Proximity distance"
msgstr "Distanza di prossimità"

msgid "Radius for circles"
msgstr "Raggio dei cerchi"

msgid "Radius of our big earth (default 6371000 meters)"
msgstr "Raggio della Terra (predef. 6371000 metri)"

msgid "Read control points as waypoint/route/none"
msgstr "Leggi i punti di controllo come waypoint/route/none"

msgid "Read/Write date format (i.e. DDMMYYYY)"
msgstr "Formato data in lettura/scrittura (per es. DDMMYYYY)"

msgid "Read/Write date format (i.e. yyyy/mm/dd)"
msgstr "Formato data in lettura/scrittura (per es. yyyy/mm/dd)"

msgid "Read/write GPGGA sentences"
msgstr "Leggi/scrivi frasi GPGGA"

msgid "Read/write GPGSA sentences"
msgstr "Leggi/scrivi frasi GPGSA"

msgid "Read/write GPRMC sentences"
msgstr "Leggi/scrivi frasi GPRMC"

msgid "Read/write GPVTG sentences"
msgstr "Leggi/scrivi frasi GPVTG"

msgid "Read/Write time format (i.e. HH:mm:ss xx)"
msgstr "Formato ora in lettura/scrittura (per es. HH:mm:ss xx)"

msgid "Retain at most this number of position points  (0 = unlimited)"
msgstr "Mantieni almeno questo numero di punti posizione (0=illimitati)"

msgid "Return current position as a waypoint"
msgstr "Ritorna la posizione corrente sotto forma di punto d'interesse"

msgid "Road type changes"
msgstr "Modifiche al tipo di strada"

msgid "Set waypoint name to source filename."
msgstr "Imposta il nome del punto d'interesse dal nome del file in ingresso."

msgid "Shortname is MAC address"
msgstr "Il nome breve è un indirizzo MAC"

msgid "Speed in bits per second of serial port (baud=4800)"
msgstr "Velocità in bit/secondo della porta seriale (baud=4800)"

msgid "Split input into separate files"
msgstr "Suddividi l'ingresso in più file separati"

msgid "Split into multiple routes at turns"
msgstr "Suddividi in più rotte alle svolte"

msgid "Starting seed of the internal number generator"
msgstr "Seme iniziale del generatore interno di numeri"

msgid "Stealth encrypted icon name"
msgstr "Nome codificato dell'icona nascosta"

msgid "Stealth non-encrypted icon name"
msgstr "Nome non codificato dell'icona nascosta"

msgid "String to separate concatenated address fields (default=\", \")"
msgstr "Stringa per separare i campi indirizzo concatenati (predef.=\", \")"

msgid "Suppress labels on generated pins"
msgstr "Ometti le etichette sugli spilli generati"

msgid "Suppress retired geocaches"
msgstr "Ometti i geocache obsoleti"

msgid "Suppress separator lines between waypoints"
msgstr "Ometti le linee di separazione tra punti d'interesse"

msgid "Suppress use of handshaking in name of speed"
msgstr "Ometti l'uso dell'handshake per aumentare la velocità"

msgid "Suppress whitespace in generated shortnames"
msgstr "Ometti gli spazi nei nomi brevi generati"

msgid "Symbol to use for point data"
msgstr "Simbolo da usare per i punti"

msgid "Sync GPS time to computer time"
msgstr "Sincronizza l'ora del GPS con quella del computer"

msgid "Synthesize track times"
msgstr "Sintetizza gli orari delle tracce"

msgid "Target GPX version for output"
msgstr "Versione GPX del file di destinazione"

msgid "Temperature unit [c=Celsius, f=Fahrenheit]"
msgstr "Unità di temperatura [c=Celsius, f=Fahrenheit]"

msgid "The icon description is already the marker"
msgstr "La descrizione dell'icona è già il segnaposto"

msgid "Treat waypoints as icons on write"
msgstr "Considera i punti d'interesse come icone in fase di scrittura"

msgid "Type of .an1 file"
msgstr "Tipo del file .an1"

msgid "Units for altitude (f)eet or (m)etres"
msgstr "Unità di misura della quota [m per metri, f per piedi]"

msgid "Units used for names with @speed ('s'tatute or 'm'etric)"
msgstr "Untià di misura per nomi con @speed (m: metric, s: statute)"

msgid "Units used when writing comments ('s'tatute or 'm'etric)"
msgstr "Unità da usare quando si scrivono i commenti (m: metric, s: statute)"

msgid "UPPERCASE synth. shortnames"
msgstr "Nomi brevi sintetizzati in MAIUSCOLO"

msgid "Use depth values on output (default is ignore)"
msgstr "Usa i valori di profondità nella destinazione (predef: è ignora)"

msgid "Use proximity values on output (default is ignore)"
msgstr "Usa i valori di prossimità nella destinazione (predef. è ignora)"

msgid "Use shortname instead of description"
msgstr "Usa il nome breve invece della descrizione"

msgid "Use specified bitmap on output"
msgstr "Usa la bitmap specificata nella destinazione"

msgid "Version of gdb file to generate (1..3)"
msgstr "Versione del file gdb da generare (1... 3)"

msgid "Version of mapsource file to generate (3,4,5)"
msgstr "Versione del file MapSource da generare (3, 4, 5)"

msgid "Waypoint background color"
msgstr "Colore di sfondo dei punti d'interesse"

msgid "Waypoint foreground color"
msgstr "Colore dei punti d'interesse"

msgid "Waypoint type"
msgstr "Tipo dei punti d'interesse"

msgid "Width in pixels of map"
msgstr "Larghezza in pixel della mappa"

msgid "Width of lines, in pixels"
msgstr "Larghezza delle linee, in pixel"

msgid "Write additional node tag key/value pairs"
msgstr "Scrivi coppie addizionali di tag chiave/valore per i nodi"

msgid "Write additional way tag key/value pairs"
msgstr "Scrivi coppie addizionali di tag chiave/valore per le vie"

msgid "Write all tracks into one file"
msgstr "Scrivi tutte le tracce in un solo file"

msgid "Write description to address field"
msgstr "Scrivi la descrizione del campo indirizzo"

msgid "Write each waypoint in a separate file"
msgstr "Scrivi ciascun punto d'interesse in un file separato"

msgid "Write notes to address field"
msgstr "Scrivi le note nel campo indirizzo"

msgid "Write position to address field"
msgstr "Scrivi la posizione del campo indirizzo"

msgid "Write position using this grid."
msgstr "Scrivi la posizione usando questa griglia"

msgid "Write timestamps with offset x to UTC time"
msgstr "Scrivi date e ore con offset X rispetto al tempo UTC"

msgid "Write tracks compatible with Carto Exploreur"
msgstr "Scrivi le tracce compatibili con Carto Exploreur"

msgid "Write tracks for Gisteq Phototracker"
msgstr "Scrivi le tracce per Gisteq Phototracker"

msgid "Zoom level to reduce points"
msgstr "Livello di zoom per rigurre i punti"

